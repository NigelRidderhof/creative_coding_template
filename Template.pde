class Template {
  int eN;
  String eT;
  String sN;
  String d;
  
  PFont f;
  
  Template (int experimentNummer, String experimentTitel, String studentNaam, String datum){
    eN = experimentNummer;
    eT = experimentTitel;
    sN = studentNaam;
    d = datum;
  }

void draw (){
    f = createFont("Roboto-Regular.ttf", 12, true);
    textFont(f);
    fill(255);
    
    textAlign(LEFT);
    text("Experiment #" + eN, 5, 16);
    
    textAlign(RIGHT);
    text(eT,width - 5, 16);
    
    textAlign(LEFT);
    text(d, 5, height / 2);
    
    textAlign(LEFT);
    text("Creative Coding S3", 5, height - 7);
    
    textAlign(RIGHT);
    text(sN, width - 5, height - 7);
  }
}
