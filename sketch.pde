int expNummer = 3;                                            //Vul in experiment nummer
String expTitel = "Image changes based on music";             //Vul in experiment titel inclusief soort experiment (sound / image / movement)
String studentNaam = "Nigel Ridderhof";                       //Vul in je naam
String datum = "11/12/2018";                                  // Vul in datum van experiment

Template tempie;

void setup(){
  size(540,675);
  background(0);
  tempie = new Template(expNummer, expTitel, studentNaam, datum);
}

void draw(){
  background(0);
  
  //Doe hier wat je moet doen. "tempie.draw();" moet als laatst in de draw uitgevoerd worden.
   
   
   
   tempie.draw();
}
